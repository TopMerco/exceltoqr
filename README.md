# README #

### A cosa serve? ###

Piccolo programma per passare da una tabella Excel a dei codici QR.

### Come lo faccio partire? ###

Per avviare il programma è necessario:
-Python 3.5.2
Dipendenze di python:
-pandas
-qrcode

### Non ho capito... Come lo faccio partire? ###

Per prima cosa è necessario installare Python 3.5.2 da qua: http://www.python.it/download/

Poi è necessario installare pip, seguire questa guida: https://www.sparkblog.org/installare-pip-python-in-windows/

Una volta installato pip, digitare i seguenti comandi nel terminale:

pip install qrcode

e, dopo aver premuto inviato ed aspettato la fine dell'esecuzione del programma

pip install pandas

E' necessario infine salvare il file qr.py linkato sopra il README.  Per avviare il programma, basterà aprire una finestra di terminale, recarsi nella cartella dove è presente il file qr.py prima salvato, e digitare "python qr.py".

### Non ho ancora capito... ###
Il programma non è semplice da usare in quanto fatto in fretta. Non dispone di interfaccia grafica per utente.